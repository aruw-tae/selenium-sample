package pe.test.aut;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import pe.test.aut.driver.factory.DriverFactory;
import pe.test.aut.util.MainProperties;

import java.util.Properties;

public class BaseTest {
    protected WebDriver driver;
    protected Properties properties;
    protected final static Logger logger = LoggerFactory.getLogger(BaseTest.class);
    protected String receiver;
    protected String sender;
    protected String password;

    @BeforeClass
    public void setUpAll() throws Exception {

        properties = MainProperties.getInstance();
        System.out.println(properties);

        DriverFactory df = new DriverFactory();
        driver = df.getDriver(System.getProperty("driverType"));

        sender=properties.getProperty("email.user");
        password=properties.getProperty("email.pass");
        receiver=properties.getProperty("email.to");



    }


}
