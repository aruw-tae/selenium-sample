package pe.test.aut.driver.factory;


import org.openqa.selenium.WebDriver;
import pe.test.aut.util.DriverUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class DriverFactory {

    final static Map<String, Supplier<WebDriver>> map = new HashMap<>();
    static {
        map.put("FIREFOX", DriverUtil::getFirefoxDriver);
        map.put("CHROME", DriverUtil::getChromeDriver);
    }

    public WebDriver getDriver(String driverType){
        Supplier<WebDriver> webDriverSupplier = map.get(driverType.toUpperCase());
        if(webDriverSupplier != null) {
            return webDriverSupplier.get();
        }
        throw new IllegalArgumentException("No such driver " + driverType.toUpperCase());
    }

}
