package pe.test.aut.mail.client;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import pe.test.aut.http.client.HttpClientHelper;
import pe.test.aut.model.Message;
import pe.test.aut.model.MessageResponse;
import pe.test.aut.util.GenericUtil;
import pe.test.aut.util.MainProperties;

import java.io.IOException;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MailClient {

    private HttpClientHelper cliente;

    public MailClient() {
        this.cliente = new HttpClientHelper();
    }

    public List<Message> getMessages(String from, Predicate<Message> filter) throws IOException {
        cliente.setupGet(MainProperties.getInstance().getProperty("service.mail.get.messages").replace("<FROM>",from));
        String message = cliente.execute();
        System.out.println(message);
        MessageResponse messageResponse = GenericUtil.parseJSON( message,MessageResponse.class);
        return messageResponse.getMessages().stream().filter(filter).collect(Collectors.toList());
    }




}
