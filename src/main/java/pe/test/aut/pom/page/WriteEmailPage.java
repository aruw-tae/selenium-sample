package pe.test.aut.pom.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pe.test.aut.pom.BaseModel;

public class WriteEmailPage extends BaseModel {
    //private By writeLocator = By.xpath("//div[@role='button'  and contains(text(),'Redactar')]");
    @FindBy(xpath = "//div[@role='button'  and contains(text(),'Redactar')]")
    private WebElement writeButton;

    //private By toTextAreaLocator = By.name("to");
    @FindBy(name = "to")
    private WebElement toTextArea;

    //private By subjectBox = By.name("subjectbox");
    @FindBy(name="subjectbox")
    private WebElement subjectBoxElement;

    //private By newMessageTitle = By.xpath("//div[contains(text(),'Mensaje nuevo')]");
    @FindBy(xpath = "//div[contains(text(),'Mensaje nuevo')]")
    private WebElement newMessageTittleElement;

    //private By bodyMessage = By.xpath("//div[@contenteditable='true'  and @role='textbox'  and @aria-label='Cuerpo del mensaje']");
    @FindBy(xpath = "//div[@contenteditable='true'  and @role='textbox'  and @aria-label='Cuerpo del mensaje']")
    private WebElement bodyMessageElement;

    //private By sentButton = By.xpath("//div[@role='button'  and contains(text(),'Enviar')]");
    @FindBy(xpath = "//div[@role='button'  and contains(text(),'Enviar')]")
    private WebElement sentButton;

    //private By linkViewMessage = By.id("link_vsm");
    @FindBy(id = "link_vsm")
    private WebElement linkedViewMessage;

    public WriteEmailPage(WebDriver driver){
        super(driver);
    }

    public void sendEmail(String email,String subject, String message){

        waitUntilElementIsClickable(By.xpath("//div[@role='button'  and contains(text(),'Redactar')]"));
        click(writeButton);

        waitUntilElementExist(By.xpath("//div[contains(text(),'Mensaje nuevo')]"));

        type(email,toTextArea);
        type(subject,subjectBoxElement);
        type(message,bodyMessageElement);
        click(sentButton);
        waitUntilElementExist(By.id("link_vsm"));

    }
}
