package pe.test.aut.pom.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pe.test.aut.pom.BaseModel;

public class LoginPage  extends BaseModel {

    @FindBy(name = "identifier")
    private WebElement emailText;

    @FindBy(id = "identifierNext")
    private WebElement nextButton;

    @FindBy(name = "password")
    private WebElement passwordLocator;

    @FindBy(id = "passwordNext")
    private WebElement passwordNextButton;

    @FindBy(id = ":1")
    private WebElement mainContainre;

    public LoginPage(WebDriver driver){
        super(driver);
    }

    public void initFactory(){
        PageFactory.initElements(driver,this);
    }

    public void makeLogin(String email, String password){

        waitUntilElementExist(By.name( "identifier"));
        type(email,emailText);
        click(nextButton);
        waitUntilElementIsClickable(By.id("passwordNext"));
        type(password,passwordLocator);
        click(passwordNextButton);
        waitUntilElementExist( By.id(":1"));

    }

}
