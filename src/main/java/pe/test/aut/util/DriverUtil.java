package pe.test.aut.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;

public class DriverUtil {

    public static WebDriver getChromeDriver(){
        WebDriver driver = null;

        StringBuffer sb = new StringBuffer();
        sb.append(System.getProperty("user.home")).append("/WebDriver/chrome/chromedriver");
        System.setProperty("webdriver.chrome.driver", sb.toString());
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


        return driver;

    }

    public static WebDriver getFirefoxDriver(){
        WebDriver driver = null;

        StringBuffer sb = new StringBuffer();
        sb.append(System.getProperty("user.home")).append("/WebDriver/firefox/geckodriver");
        System.setProperty("webdriver.gecko.driver", sb.toString());
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


        return driver;

    }
}
