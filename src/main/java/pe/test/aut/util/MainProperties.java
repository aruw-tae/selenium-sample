package pe.test.aut.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MainProperties {
    protected final static Logger logger = LoggerFactory.getLogger(MainProperties.class);


    private final static Properties properties = new Properties();
    static {
        try (InputStream input = new FileInputStream(Thread.currentThread().getContextClassLoader()
                .getResource("general.properties").getFile())) {

            properties.load(input);

        } catch (IOException ex) {
           ex.printStackTrace();
        }
    }

    private MainProperties(){
    }

    public static Properties getInstance(){
        return properties;
    }
}
