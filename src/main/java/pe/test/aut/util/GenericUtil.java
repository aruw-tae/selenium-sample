package pe.test.aut.util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

public class GenericUtil {

    public static String getOS(){
        String os = "linux";
        if (System.getProperty("os.name").toUpperCase().contains("MAC")) {
            os = "mac";
        }
        return os;
    }

    public static <T> T parseJSON(String source,Class clazz){
        return (T)new Gson().fromJson(source,clazz);
    }

    public static <T> T parseJSON(JsonElement el, Class clazz){
        return (T)new Gson().fromJson(el,clazz);
    }

}
