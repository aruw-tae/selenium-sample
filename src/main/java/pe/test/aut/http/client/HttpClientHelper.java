package pe.test.aut.http.client;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import javax.swing.text.html.Option;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Optional;


public class HttpClientHelper {

    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private OkHttpClient client;
    private Request request;


    public HttpClientHelper() {
        this.client = new OkHttpClient();
    }

    public void setupGet(String url) {
        this.request = new Request.Builder()
                .url(url)
                .build();

    }

    public void setupPost(String url, Optional<String> bodyRequest){

    }

    public String execute() throws IOException {
        Response response = this.client.newCall(this.request).execute();
        return  response.body().string();
    }


}
